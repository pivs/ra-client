package http

import (
	"bytes"
	"crypto/x509"
	"encoding/asn1"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/pkg/errors"

	"github.hpe.com/pivs/ra-common.git/cryptoutil"
	"github.hpe.com/pivs/ra-common.git/protocol/enroll"
	"github.hpe.com/pivs/tss"
)

func init() {
	enroll.RegisterProvider("https", httpProvider)
	enroll.RegisterProvider("http", httpProvider)
}

func httpProvider(uri *url.URL) (enroll.Service, error) {
	return &httpService{*uri, http.DefaultClient}, nil
}

type badStatusError struct {
	status string
	code   int
}

func (e badStatusError) Error() string {
	return fmt.Sprintf("bad status: %d %s", e.code, e.status)
}

type httpService struct {
	basePath url.URL
	client   *http.Client
}

func (s *httpService) post(phase string, body io.Reader) (*http.Response, error) {
	u := s.basePath
	q := u.Query()
	q.Set("p", phase)
	u.RawQuery = q.Encode()
	return s.client.Post(u.String(), "application/json", body)
}

func (s *httpService) request(phase string, req, res interface{}) error {
	buf := new(bytes.Buffer)
	enc := json.NewEncoder(buf)
	err := enc.Encode(req)
	if err != nil {
		return err
	}

	httpResp, err := s.post(phase, buf)
	if err != nil {
		return err
	}

	defer httpResp.Body.Close()

	if httpResp.StatusCode != 200 {
		ctype := httpResp.Header.Get("Content-Type")
		if ctype == "application/json" {
			jdec := json.NewDecoder(httpResp.Body)
			var jerr enroll.ErrorResponse
			if err := jdec.Decode(&jerr); err != nil {
				return errors.Wrap(err, "could not decode error response")
			}

			return errors.New(jerr.Error)
		}

		return errors.New(httpResp.Status)
	}

	if res != nil {
		dec := json.NewDecoder(httpResp.Body)
		return dec.Decode(res)
	}

	return nil
}

func (s *httpService) Enroll(r enroll.Registration, activator enroll.Activator) (*x509.Certificate, error) {
	subject, err := asn1.Marshal(r.Subject)
	if err != nil {
		return nil, err
	}

	ekbuf := new(bytes.Buffer)
	_, err = tss.Write_TPMT_PUBLIC(ekbuf, r.EndorsementKey)
	if err != nil {
		return nil, err
	}

	akbuf := new(bytes.Buffer)
	_, err = tss.Write_TPMT_PUBLIC(akbuf, r.AttestationKey)
	if err != nil {
		return nil, err
	}

	var res enroll.BeginResponse
	err = s.request("begin", enroll.BeginRequest{
		Subject:                subject,
		EndorsementKey:         ekbuf.Bytes(),
		EndorsementCertificate: r.EndorsementCertificate.Raw,
		AttestationKey:         akbuf.Bytes(),
	}, &res)
	if err != nil {
		return nil, err
	}

	cred, err := activator(res.Secret, res.Credential)
	if err != nil {
		return nil, err
	}

	akCertData, err := cryptoutil.AesDecrypt(cred, res.Certificate)
	if err != nil {
		return nil, err
	}

	akCert, err := x509.ParseCertificate(akCertData)
	if err != nil {
		return nil, err
	}

	err = s.request("complete", enroll.CompleteRequest{
		ID:          res.ID,
		Certificate: akCertData,
	}, nil)
	if err != nil {
		return nil, err
	}

	return akCert, nil
}
