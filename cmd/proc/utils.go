package main

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"math/big"
	"time"

	"github.hpe.com/pivs/tss"
)

func loadCertificate(path string) (*x509.Certificate, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	p, _ := pem.Decode(data)
	return x509.ParseCertificate(p.Bytes)
}

func loadPrivateKey(path string, password []byte) (crypto.PrivateKey, error) {
	key_bin, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	key_block, _ := pem.Decode(key_bin)
	key_der := key_block.Bytes
	if x509.IsEncryptedPEMBlock(key_block) {
		key_der, err = x509.DecryptPEMBlock(key_block, password)
		if err != nil {
			return nil, err
		}
	}

	var key crypto.PrivateKey
	switch key_block.Type {
	case "RSA PRIVATE KEY":
		key, err = x509.ParsePKCS1PrivateKey(key_der)

	case "PRIVATE KEY":
		key, err = x509.ParsePKCS8PrivateKey(key_der)

	default:
		return nil, fmt.Errorf("Unsupported priv type: %s", key_block.Type)
	}

	if err != nil {
		return nil, err
	}

	return key, nil
}

func createCertificate(caCert *x509.Certificate, caKey crypto.PrivateKey, hostname string, pub *tss.TPMT_PUBLIC) ([]byte, error) {
	now := time.Now()
	template := x509.Certificate{
		Subject:   pkix.Name{CommonName: hostname},
		NotBefore: now,
		NotAfter:  now.AddDate(10, 0, 0),
	}

	var pubkey crypto.PublicKey
	switch pub.Type {
	case tss.TPM_ALG_RSA:
		modulus := []byte(*pub.Unique.(*tss.PUBLIC_KEY_RSA))

		// To create a unique serial number, hash the key to be certified
		unique_sha1 := sha1.Sum(modulus)
		template.SerialNumber = new(big.Int).SetBytes(unique_sha1[:])

		pubkey = &rsa.PublicKey{
			N: big.NewInt(0).SetBytes(modulus),
			E: 0x10001,
		}

	default:
		return nil, fmt.Errorf("Public key algorithm %s not supported", pub.Type)
	}

	return x509.CreateCertificate(rand.Reader, &template, caCert, pubkey, caKey)
}
