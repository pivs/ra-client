package main

import (
	"bytes"
	"crypto/x509/pkix"
	"encoding/binary"
	"flag"
	"log"
	"net/url"
	"regexp"
	"strconv"

	"github.com/sirupsen/logrus"

	"github.com/pkg/errors"

	"github.hpe.com/pivs/tss"
	"github.hpe.com/pivs/tsslib"
	_ "github.hpe.com/pivs/tsslib-ibm"

	_ "github.hpe.com/pivs/ra-client.git/enroll/http"
	"github.hpe.com/pivs/ra-common.git/protocol/enroll"
)

var (
	enrollService enroll.Service
	context       tsslib.TSS_CONTEXT
	enrollUri     *url.URL
	hEK           tss.TPM_HANDLE
	hAK           tss.TPM_HANDLE
	subject       pkix.Name
	nvIndex       uint64
)

func parseDistinguishedName(dn *pkix.Name, subj string) error {
	re := regexp.MustCompile("/([^=]+)=([^/]+)")
	for _, r := range re.FindAllStringSubmatch(subj, -1) {
		key, val := r[1], r[2]
		switch key {
		case "C":
			dn.Country = []string{val}

		case "O":
			dn.Organization = []string{val}

		case "OU":
			dn.OrganizationalUnit = []string{val}

		case "L":
			dn.Locality = []string{val}

		case "P":
			dn.Province = []string{val}

		case "SA":
			dn.StreetAddress = []string{val}

		case "PC":
			dn.PostalCode = []string{val}

		case "SN":
			dn.SerialNumber = val

		case "CN":
			dn.CommonName = val
		}
	}

	return nil
}

func parseArgs() error {
	var (
		enrollUriArg = flag.String("uri", "", "Enrollment service URI")
		subjectArg   = flag.String("subject", "", "Subject for the new AK certificate")
		indexArg     = flag.String("nvindex", "1c00012", "NV index to store the AK certificate")
	)
	flag.Parse()

	index, err := strconv.ParseUint(*indexArg, 16, 0)
	if err != nil {
		return errors.Wrap(err, "can't parse NV index")
	}

	if *subjectArg == "" {
		return errors.New("subject can't be empty")
	}

	err = parseDistinguishedName(&subject, *subjectArg)
	if err != nil {
		return errors.Wrap(err, "can't parse subject string")
	}

	if *enrollUriArg == "" {
		return errors.New("enrollment service URI can't be empty")
	}

	uri, err := url.Parse(*enrollUriArg)
	if err != nil {
		return errors.Wrap(err, "can't parse service URI")
	}

	enrollUri = uri
	nvIndex = index
	return nil
}

func setupResources() error {
	lib := tsslib.IBMTSS

	log.Print("Creating TSS context...")
	ctx, err := lib.CreateContext()
	if err != nil {
		return errors.Wrap(err, "can't create TSS context")
	}

	service, err := enroll.New(enrollUri.String())
	if err != nil {
		return errors.Wrap(err, "enrollment service creation failed")
	}

	enrollService = service
	context = ctx
	return nil
}

func createPolicySession() (hSession tss.TPM_HANDLE, err error) {
	sasOut, err := context.StartAuthSession(&tsslib.StartAuthSession_In{
		TpmKey:      tss.TPM_HANDLE(tss.TPM_RH_NULL),
		SessionType: tss.TPM_SE_POLICY,
		Bind:        tss.TPM_HANDLE(tss.TPM_RH_NULL),
		AuthHash:    tss.TPM_ALG_SHA256,
		Symmetric: tss.TPMT_SYM_DEF{
			Algorithm: tss.TPM_ALG_XOR,
			KeyBits:   uint16(tss.TPM_ALG_SHA256),
			Mode:      tss.TPM_ALG_NULL,
		},
	})
	if err != nil {
		return hSession, err
	}

	_, err = context.PolicySecret(&tsslib.PolicySecret_In{
		PolicySession: sasOut.SessionHandle,
		AuthHandle:    tss.TPM_HANDLE(tss.TPM_RH_ENDORSEMENT),
	})
	if err != nil {
		context.FlushContext(sasOut.SessionHandle)
		return hSession, err
	}

	return sasOut.SessionHandle, nil
}

func activator(secret, credential []byte) ([]byte, error) {
	logrus.Info("Starting policy session")
	hSession, err := createPolicySession()
	if err != nil {
		return nil, errors.Wrap(err, "policy session startup failed")
	}

	defer context.FlushContext(hSession)

	logrus.Info("Session: ", hSession)
	in := tsslib.ActivateCredential_In{
		KeyHandle:      hEK,
		ActivateHandle: hAK,
		Secret:         secret,
		CredentialBlob: credential,
	}

	logrus.Infof("Activate: %x", in)
	out, err := context.ActivateCredential(&in, tsslib.WithHandle(tss.TPM_HANDLE(tss.TPM_RS_PW)), tsslib.WithHandle(hSession))
	if err != nil {
		logrus.Errorf("context.ActivateCredential: %v", err)
		return nil, err
	}

	return out.CertInfo, nil
}

// TODO: Remove me
func ComputeName(pub *tss.TPMT_PUBLIC) (tss.NAME, error) {
	halg, err := pub.NameAlg.Hash()
	if err != nil {
		return nil, err
	}

	hfunc := halg.New()
	if _, err = tss.Write_TPMT_PUBLIC(hfunc, *pub); err != nil {
		return nil, err
	}

	var alg [2]byte
	binary.BigEndian.PutUint16(alg[:], uint16(pub.NameAlg))
	return hfunc.Sum(alg[:]), nil
}

func main() {
	if err := parseArgs(); err != nil {
		logrus.Error(err)
		return
	}

	if err := setupResources(); err != nil {
		logrus.Error(err)
		return
	}

	defer context.Delete()

	log.Print("Loading RSA Endorsement Certificate...")
	ekCert, err := tsslib.LoadCertificateFromIndex(context, 0x1c00002)
	if err != nil {
		logrus.Errorf("could not load certificate: %v", err)
		return
	}

	var pubEK, pubAK *tss.TPMT_PUBLIC
	log.Print("Generating RSA Endorsement Key...")
	hEK, pubEK, err = tsslib.CreateEndorsementKey(context)
	if err != nil {
		logrus.Errorf("can't create endorsement key: %v", err)
		return
	}

	defer context.FlushContext(hEK)

	log.Print("Generating RSA Attestation Key...")
	hAK, pubAK, err = tsslib.CreateAttestationKey(context, nil)
	if err != nil {
		logrus.Errorf("can't create attestation key: %v", err)
		return
	}

	defer context.FlushContext(hAK)

	var b bytes.Buffer
	tss.Write_TPMT_PUBLIC(&b, *pubAK)
	tsslib.WriteFile2B("akpub2.bin", b.Bytes())

	log.Print("Connecting to enrollment service...")
	akCert, err := enrollService.Enroll(enroll.Registration{
		Subject:                subject,
		EndorsementKey:         *pubEK,
		EndorsementCertificate: ekCert,
		AttestationKey:         *pubAK,
	}, activator)
	if err != nil {
		logrus.Errorf("enrollment failed: %v", err)
		return
	}

	log.Println(akCert)

	// log.Print("Generating certificate for the generated AK...")
	// certAK, err := createCertificate(pcaCert, pcaKey, hostname, pubAK)
	// if err != nil {
	// 	log.Printf("ERR: Can't create a signed Attestation Key: %s", err)
	// 	return
	// }

	// if len(certAK) > 0xFFFF {
	// 	log.Print("ERR: AK certificate is too big")
	// 	return
	// }

	// log.Printf("Creating a NV space %08x to hold %d bytes from AK certificate...", nvIndex, len(certAK))
	// nvIndex := tss.TPMI_RH_NV_INDEX(nvIndex)
	// err = context.NV_DefineSpace(&tsslib.NV_DefineSpace_In{
	// 	AuthHandle: tss.TPM_RH_PLATFORM,
	// 	PublicInfo: tss.TPMS_NV_PUBLIC{
	// 		NvIndex: nvIndex,
	// 		NameAlg: tss.TPM_ALG_SHA256,
	// 		Attributes: tss.TPMA_NVA_PPWRITE |
	// 			tss.TPMA_NVA_ORDINARY |
	// 			tss.TPMA_NVA_WRITEDEFINE |
	// 			tss.TPMA_NVA_PPREAD |
	// 			tss.TPMA_NVA_OWNERREAD |
	// 			tss.TPMA_NVA_AUTHREAD |
	// 			tss.TPMA_NVA_NO_DA |
	// 			tss.TPMA_NVA_PLATFORMCREATE,
	// 		AuthPolicy: tss.DIGEST{},
	// 		DataSize:   uint16(len(certAK)),
	// 	},
	// })

	// if err != nil {
	// 	log.Printf("ERR: Can't create a NV space: %s", err)
	// 	return
	// }

	// log.Printf("Writting %d bytes to NV index %08x...", len(certAK), index)
	// err = tsslib.NvWrite(ctx, nvIndex, tss.TPM_RH_PLATFORM, certAK, 0)

	// if err != nil {
	// 	log.Printf("ERR: Can't write to NV index: %s", err)
	// 	return
	// }

	log.Println("DONE")
}
