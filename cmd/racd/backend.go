package main

import (
	"github.hpe.com/pivs/tss"
	"github.hpe.com/pivs/tsslib"
)

type ClientBackend struct {
	Context        tsslib.TSS_CONTEXT
	LoadedAK       tss.TPM_HANDLE
	AttestationKey tss.TPMT_PUBLIC
}

func (c ClientBackend) Close() error {
	return c.Context.Delete()
}

func (c ClientBackend) Quote(nonce []byte, selection tss.TPML_PCR_SELECTION) (*tsslib.QuoteInfo, error) {
	return c.Context.Quote(c.LoadedAK, nonce, selection)
}

func (c ClientBackend) ReadPCRs(selectionIn tss.PCR_SELECTION_MASK, hash tss.TPM_ALG_ID) (uint, []tss.DIGEST, error) {
	var digests []tss.DIGEST
	var uc uint

retry:
	for retries := 5; retries > 0; retries-- {
		digests = make([]tss.DIGEST, 0, 24)
		first := true

		selection := selectionIn
		for selection != 0 {
			info, err := c.Context.PCR_Read(tss.TPML_PCR_SELECTION{{Hash: hash, Select: selection}})
			if err != nil {
				return 0, nil, err
			}

			if first {
				uc = info.UpdateCounter
			} else if uc != info.UpdateCounter {
				continue retry
			}

			// Clear out the PCRs returned in this call
			sel := info.Selection[0]
			selection ^= sel.Select

			digests = append(digests, info.Digests...)
		}

		break
	}

	return uc, digests, nil
}
