package main

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"unsafe"

	"golang.org/x/net/context"

	"github.hpe.com/pivs/ra-common.git/ima"
	pb "github.hpe.com/pivs/ra-common.git/proto"
	"github.hpe.com/pivs/ra-common.git/tcg"

	"github.hpe.com/pivs/tss"
	"github.hpe.com/pivs/tsslib"
	_ "github.hpe.com/pivs/tsslib-ibm"
)

type attesterServer struct {
	Backend ClientBackend

	IntegrityLogPath string
	BootLogPath      string
}

func (s *attesterServer) getQuote(nonce []byte, algorithm tss.TPM_ALG_ID, pcrMask tss.PCR_SELECTION_MASK) (*tsslib.QuoteInfo, []tss.DIGEST, error) {
	selection := tss.TPML_PCR_SELECTION{{
		Hash:   algorithm,
		Select: pcrMask,
	}}

	info, err := s.Backend.Quote(nonce, selection)
	if err != nil {
		return nil, nil, err
	}

	// TODO: We need to check for race-conditions between read and quote
	_, digests, err := s.Backend.ReadPCRs(pcrMask, algorithm)
	if err != nil {
		return nil, nil, err
	}

	return info, digests, nil
}

func (s *attesterServer) buildMeasuredData(nonce []byte, algorithm tss.TPM_ALG_ID, pcrMask tss.PCR_SELECTION_MASK, events [][]byte) (*pb.MeasuredData, error) {
	quoteInfo, digests, err := s.getQuote(nonce, algorithm, pcrMask)
	if err != nil {
		return nil, err
	}

	var sigbuf bytes.Buffer
	_, err = tss.Write_TPMT_SIGNATURE(&sigbuf, quoteInfo.Signature)
	if err != nil {
		return nil, err
	}

	var quotebuf bytes.Buffer
	_, err = tss.Write_TPMS_ATTEST(&quotebuf, quoteInfo.Quoted)
	if err != nil {
		return nil, err
	}

	return &pb.MeasuredData{
		Algorithm: uint32(algorithm),
		Digests:   dig2bytes(digests),
		Events:    events,
		Attested:  quotebuf.Bytes(),
		Signature: sigbuf.Bytes(),
	}, nil
}

func (s *attesterServer) GetIntegrityData(ctx context.Context, data *pb.IntegrityDataRequest) (*pb.MeasuredData, error) {
	events, err := ima.ReadMeasurements(s.IntegrityLogPath)
	if err != nil {
		return nil, err
	}

	events = events[data.Offset:]

	rawEvents := make([][]byte, len(events))

	for i, event := range events {
		var buf bytes.Buffer
		_, err = event.Marshal(&buf)
		if err != nil {
			return nil, fmt.Errorf("Error during event serialization: %s", err.Error())
		}

		rawEvents[i] = buf.Bytes()
	}

	acceptedAlgorithms, err := toAlgorithms(data.AcceptedAlgorithms)
	if err != nil {
		return nil, err
	}

	selectedAlgorithm, err := chooseAlgorithm(s.Backend.Context, acceptedAlgorithms)
	if err != nil {
		return nil, fmt.Errorf("Algorithm negotiation failed: %s", err.Error())
	}

	return s.buildMeasuredData(data.Nonce, selectedAlgorithm, tss.PCR10, rawEvents)
}

func (s *attesterServer) GetBootData(ctx context.Context, data *pb.BootDataRequest) (*pb.MeasuredData, error) {
	info, err := tcg.Read_BootEventsTPM2(s.BootLogPath)
	if err != nil {
		return nil, err
	}

	events := make([][]byte, len(info.Events))

	for i, event := range info.Events {
		var buf bytes.Buffer
		_, err = event.Marshal(&buf)
		if err != nil {
			return nil, err
		}

		events[i] = buf.Bytes()
	}

	// FIXME: Need to create a proper mechanism to select the algorithm
	selectedAlgorithm := tss.TPM_ALG_SHA256

	return s.buildMeasuredData(data.Nonce, selectedAlgorithm, tss.SRTM_PCRs, events)
}

func (s *attesterServer) GetIdentity(ctx context.Context, in *pb.IdentityRequest) (*pb.Identity, error) {
	if len(in.Unique) > 1 {
		return nil, errors.New("invalid unique value")
	}

	var buf bytes.Buffer
	_, err := tss.Write_TPMT_PUBLIC(&buf, s.Backend.AttestationKey)
	if err != nil {
		return nil, err
	}

	return &pb.Identity{Public: buf.Bytes()}, nil
}

func dig2bytes(digests []tss.DIGEST) [][]byte {
	return *(*[][]byte)(unsafe.Pointer(&digests))
}

func SetupServer() pb.AttesterServer {
	lib := tsslib.IBMTSS
	ctx, err := lib.CreateContext()
	if err != nil {
		panic(err)
	}

	log.Println("Generating AK...")
	handle, akpub, err := tsslib.CreateAttestationKey(ctx, nil)
	if err != nil {
		panic(err)
	}

	backend := ClientBackend{
		Context:        ctx,
		LoadedAK:       handle,
		AttestationKey: *akpub,
	}

	return &attesterServer{
		Backend: backend,
		//IntegrityLogPath: "shared/imasig.log",
		IntegrityLogPath: "binary_runtime_measurements",
		BootLogPath:      "tpm2bios.log",
	}
}
