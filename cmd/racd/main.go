package main

import (
	"flag"
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	pb "github.hpe.com/pivs/ra-common.git/proto"
)

func main() {
	port := flag.Uint("port", 2424, "")
	flag.Parse()

	log.Printf("Starting server at port %d...", *port)
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	pb.RegisterAttesterServer(grpcServer, SetupServer())

	log.Println("Serving...")
	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatal("grpc error: ", err)
	}
}
