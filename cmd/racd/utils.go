package main

import (
	"fmt"

	"github.hpe.com/pivs/tss"
	"github.hpe.com/pivs/tsslib"
)

func toAlgorithms(ids []uint32) ([]tss.TPM_ALG_ID, error) {
	tssAlgs := make([]tss.TPM_ALG_ID, len(ids))
	for i, algID := range ids {
		if algID > 0xFFFF {
			return nil, fmt.Errorf("Bad value for algorithm: %04X", algID)
		}

		tssAlgs[i] = tss.TPM_ALG_ID(algID)
	}

	return tssAlgs, nil
}

func chooseAlgorithm(ctx tsslib.TSS_CONTEXT, acceptedAlgs []tss.TPM_ALG_ID) (alg tss.TPM_ALG_ID, err error) {
	if len(acceptedAlgs) == 0 {
		err = fmt.Errorf("No algorithms left to continue negotiation")
		return
	}

	for _, calg := range acceptedAlgs {
		if (calg.Info().Type & tss.ALG_TYPE_H) == 0 {
			err = fmt.Errorf("Not a hash algorithm: %s", calg)
			return
		}
	}

	banks, err := tsslib.ActiveBanks(ctx)
	if err != nil {
		return
	}

	for _, ca := range acceptedAlgs {
		for _, ba := range banks {
			if ca == ba {
				alg = ca
				return
			}
		}
	}

	err = fmt.Errorf("Algorithm negotiation failed")
	return
}
